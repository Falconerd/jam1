CC = gcc
STD = c89
LDFLAGS = -Wall -pedantic -ansi
LIBS = -lglfw -ldl -lm -lfreetype -lftgl

build: src/main.c lib/glad.c
	$(CC) -g3 $(LDFLAGS) $^ $(LIBS) -std=$(STD) -o ./a.out
