#include <assert.h>
#include <time.h>
#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include "math_linear.h"
#include "gl_helpers.h"
#include "shader.h"
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"	

#define PI 3.14159
#define SCR_WIDTH 384
#define SCR_HEIGHT 216
#define MAX_ENTITIES 100
#define SCR_SCALE 5

#define FLAG_ALIVE 1
#define FLAG_ENEMY 2
#define FLAG_CAN_SEE_PLAYER 4
#define FLAG_USES_PROJECTILES 8
#define FLAG_PROJECTILE 16
#define FLAG_HIT 32

GLFWwindow *window;
double now;
int victory = 0;

GLfloat vertices[] = {
	/* vertices		colours				tex coords */
	 0.5f,  0.5f, 0.0f,	1.0f, 1.0f, 1.0f, 1.0f, 	1.0f, 1.0f,
	 0.5f, -0.5f, 0.0f,	1.0f, 1.0f, 1.0f, 1.0f, 	1.0f, 0.0f,
	-0.5f, -0.5f, 0.0f,	1.0f, 1.0f, 1.0f, 1.0f, 	0.0f, 0.0f,
	-0.5f,  0.5f, 0.0f,	1.0f, 1.0f, 1.0f, 1.0f, 	0.0f, 1.0f
};
GLuint indices[] = {
	0, 1, 3,
	1, 2, 3
};

mat4x4 identity_matrix = {
	{ 1.0f, 0.0f, 0.0f, 0.0f },
	{ 0.0f, 1.0f, 0.0f, 0.0f },
	{ 0.0f, 0.0f, 1.0f, 0.0f },
	{ 0.0f, 0.0f, 0.0f, 1.0f }
};

struct sprite {
	GLuint tex_id;
	GLfloat width;
	GLfloat height;
};

GLuint vao;
GLuint vbo;
GLuint ebo;
GLuint shader;
GLuint texture;

struct sprite arrow_btn;
struct sprite add_btn;

struct anim {
	struct sprite *sprites[4];
	int len;
	int curr;
	float dur[4];
	float start;
};

/* crude sprite storage */
struct sprite sprites[32] = {0};

/* entity system */
unsigned next_entity = 0;
unsigned entity_count = 0;

enum entity_types {
	ENTITY_NULL,
	ENTITY_PLAYER,
	ENTITY_SKELETON_ARCHER,
	ENTITY_ZOMBIE,
	ENTITY_ARROW
};

const char *entity_type_name(enum entity_types type)
{
	switch (type) {
	case ENTITY_NULL:
		return "ENTITY_NULL";
	case ENTITY_PLAYER:
		return "ENTITY_PLAYER";
	case ENTITY_SKELETON_ARCHER:
		return "ENTITY_SKELETON_ARCHER";
	case ENTITY_ZOMBIE:
		return "ENTITY_ZOMBIE";
	case ENTITY_ARROW:
		return "ENTITY_ARROW";
	}
	return "NONE";
}

struct entity {
	unsigned type;
	float x;
	float y;
	unsigned flags;
	float speed;
	enum entity_types projectile;
	double projectile_cooldown;
	int hp;
	struct sprite *sprite;
	float vel_x;
	float vel_y;
	float rot;
	double shot_projectile_time;
};

struct entity entity_data[MAX_ENTITIES] = {0};
struct entity entity_defaults[32] = {0};

struct entity e_player = {
	ENTITY_PLAYER, 45, 45, 1, 80, ENTITY_NULL, 0, 3
};

struct entity e_skele_archer = {
	ENTITY_SKELETON_ARCHER, 0, 0, FLAG_ALIVE | FLAG_USES_PROJECTILES, 80, ENTITY_ARROW, 1.0, 2
};

struct entity e_arrow = {
	ENTITY_ARROW, 0, 0, FLAG_ALIVE | FLAG_PROJECTILE, 120.0f, ENTITY_NULL, 0, 1
};

struct sprite victory_sprite;

struct sprite anim_skele_archer_shoot[2];
struct sprite anim_player_walk[4];
struct sprite anim_player_attack[2];

unsigned player_walk_cycle = 0;
double player_step_time = 0;
double player_attack_time = 0;

int skele_count = 4;

unsigned player_id;

unsigned spawn(enum entity_types type, float x, float y)
{
	unsigned id = next_entity++;

	if (next_entity >= MAX_ENTITIES) {
		int i;
		int found = 0;
		for (i = 0; i < MAX_ENTITIES; ++i) {
			if ((entity_data[i].flags & FLAG_ALIVE) == 0) {
				id = i;
				found = 1;
				entity_count--;
				break;
			}
		}

		if (!found) {
			printf("entity array full");
			exit(-1);
		}
	}

	entity_data[id] = entity_defaults[type];
	entity_data[id].flags |= FLAG_ALIVE;
	entity_data[id].x = x;
	entity_data[id].y = y;
	/* entity_data[id].sprite = &sprites[type]; */
	entity_count++;
	printf("spawned %s. ID: %d SPRITE ID: %d\n", entity_type_name(type), id, type);
	return id;
}

unsigned spawn_enemy(enum entity_types type, float x, float y)
{
	unsigned id = spawn(type, x, y);
	entity_data[id].flags |= FLAG_ENEMY;
	return id;
}

/* timer stuff... each entity gets 1 yay */
double timers[MAX_ENTITIES] = {0};

void framebuffer_size_callback(GLFWwindow *window, int width, int height)
{
	glViewport(0, 0, width, height);
}

void process_input(GLFWwindow *window)
{
	float x = 0;
	float y = 0;
	if (GLFW_PRESS == glfwGetKey(window, GLFW_KEY_ESCAPE))
		glfwSetWindowShouldClose(window, GLFW_TRUE);

	if (GLFW_PRESS == glfwGetKey(window, GLFW_KEY_SPACE) && player_attack_time + 0.35 < now)
		player_attack_time = now;

	if (GLFW_PRESS == glfwGetKey(window, GLFW_KEY_F)) {
		y = 1;
	} else if (GLFW_PRESS == glfwGetKey(window, GLFW_KEY_S)) {
		y = -1;
	}

	if (GLFW_PRESS == glfwGetKey(window, GLFW_KEY_R)) {
		x = -1;
	} else if (GLFW_PRESS == glfwGetKey(window, GLFW_KEY_T)) {
		x = 1;
	}

	if (0 != x && 0 != y) {
		x *= 0.7f;
		y *= 0.7f;
	}

	entity_data[player_id].vel_y = y * entity_data[player_id].speed;
	entity_data[player_id].vel_x = x * entity_data[player_id].speed;
}

void load_sprite(struct sprite *sprite, const char *path)
{
	int w, h, nc;
	unsigned char *data = stbi_load(path, &w, &h, &nc, 0);
	if (!data) {
		printf("failed to load texture %s\n", path);
		exit(-1);
	}

	glGenTextures(1, &sprite->tex_id);
	glBindTexture(GL_TEXTURE_2D, sprite->tex_id);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, w, h, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
	glGenerateMipmap(GL_TEXTURE_2D);
	stbi_image_free(data);

	sprite->width = w;
	sprite->height = h;
}

void reset()
{
	unsigned i;
	for (i = 0; i < MAX_ENTITIES; ++i) {
		entity_data[i].flags = 0;
		timers[i] = 0;
	}
	
	next_entity = 0;
	entity_count = 0;

	player_id = spawn(ENTITY_PLAYER, 45, 45);
	entity_data[player_id] = entity_defaults[ENTITY_PLAYER];
	entity_data[player_id].sprite = &sprites[ENTITY_PLAYER];
	entity_defaults[ENTITY_ARROW] = e_arrow;
	entity_data[player_id].x = 45;
	entity_data[player_id].flags = 1;

	for (i = 0; i < 4; ++i)
		spawn_enemy(ENTITY_SKELETON_ARCHER, rand() % SCR_WIDTH, rand() % SCR_HEIGHT);
}

void win()
{
	victory = 1;
}

void init()
{
	mat4x4 proj;
	time_t t;

	stbi_set_flip_vertically_on_load(1);
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 5);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	srand((int)time(&t));

	window = glfwCreateWindow(SCR_WIDTH * SCR_SCALE, SCR_HEIGHT * SCR_SCALE, "ABT JAM", NULL, NULL);

	assert(NULL != window);

	glfwMakeContextCurrent(window);
	glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);

	assert(0 != gladLoadGLLoader((GLADloadproc)glfwGetProcAddress));

	glGenVertexArrays(1, &vao);
	glGenBuffers(1, &vbo);
	glGenBuffers(1, &ebo);

	glBindVertexArray(vao);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof vertices, vertices, GL_STATIC_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof indices, indices, GL_STATIC_DRAW);

	shader = create_shader_program("src/sprite.vs", "src/sprite.fs", NULL);

	glUseProgram(shader);

	/* tell gl how to use data */
	/* below is just for sprites */
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 9 * sizeof(GLfloat), NULL);
	glEnableVertexAttribArray(0);

	glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, 9 * sizeof(GLfloat), (void*)(3 * sizeof(GLfloat)));
	glEnableVertexAttribArray(1);

	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 9 * sizeof(GLfloat), (void*)(7 * sizeof(GLfloat)));
	glEnableVertexAttribArray(2);

	glUseProgram(shader);

	load_sprite(&arrow_btn, "assets/arrow_button.png");
	load_sprite(&add_btn, "assets/add_button.png");
	load_sprite(&victory_sprite, "assets/victory.png");
	load_sprite(&sprites[ENTITY_PLAYER], "assets/player.png");
	load_sprite(&sprites[ENTITY_SKELETON_ARCHER], "assets/skeleton_archer1.png");
	load_sprite(&sprites[ENTITY_ARROW], "assets/arrow.png");

	sprites[ENTITY_NULL].width = 0;
	sprites[ENTITY_NULL].height = 0;

	mat4x4_ortho(proj, 0, SCR_WIDTH, 0, SCR_HEIGHT, -1.0f, 1.0f);
	setUniformMatrix4fv(&shader, "projection", &proj[0][0]);

	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_BLEND);

	entity_defaults[ENTITY_PLAYER] = e_player;
	entity_defaults[ENTITY_SKELETON_ARCHER] = e_skele_archer;
	entity_defaults[ENTITY_ARROW] = e_arrow;

	entity_defaults[ENTITY_PLAYER].sprite = &sprites[ENTITY_PLAYER];
	entity_defaults[ENTITY_SKELETON_ARCHER].sprite = &sprites[ENTITY_SKELETON_ARCHER];
	entity_defaults[ENTITY_ARROW].sprite = &sprites[ENTITY_ARROW];

	load_sprite(&anim_skele_archer_shoot[0], "assets/skeleton_archer2.png");
	load_sprite(&anim_skele_archer_shoot[1], "assets/skeleton_archer3.png");

	load_sprite(&anim_player_walk[0], "assets/player2.png");
	load_sprite(&anim_player_walk[1], "assets/player3.png");
	load_sprite(&anim_player_walk[2], "assets/player4.png");
	load_sprite(&anim_player_walk[3], "assets/player5.png");
	load_sprite(&anim_player_attack[0], "assets/player6.png");
	load_sprite(&anim_player_attack[1], "assets/player7.png");

	reset();
}

void draw_sprite(struct sprite *sprite, float x, float y, float z, float rot)
{
	mat4x4 trans;
	mat4x4_identity(trans);
	mat4x4_translate(trans, x, y, z);
	mat4x4_rotate(trans, trans, 0, 0, 1.0f, rot);
	mat4x4_scale_aniso(trans, trans, sprite->height, sprite->width, 1.0f);
	setUniformMatrix4fv(&shader, "transform", &trans[0][0]);

	glBindTexture(GL_TEXTURE_2D, sprite->tex_id);
	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
}

void draw_entity(struct entity *entity)
{
	draw_sprite(entity->sprite, entity->x, entity->y, 0, entity->rot);
}

void game_loop(double dt)
{
	unsigned i;
	struct entity *player = &entity_data[player_id];

	if (victory) {
		draw_sprite(&victory_sprite, SCR_WIDTH / 2, SCR_HEIGHT / 2, 0, 0);
		return;
	}

	for (i = 0; i < entity_count; ++i) {
		struct entity *entity = &entity_data[i];
		if ((entity->flags & FLAG_ALIVE) == 0)
			continue;

		timers[i] -= dt;

		entity->x += entity->vel_x * dt;
		entity->y += entity->vel_y * dt;

		if (entity->flags & FLAG_PROJECTILE) {
			if (entity->x < -20 || entity->x > SCR_WIDTH + 20 ||
			    entity->y < -20 || entity->y > SCR_HEIGHT + 20)
				entity->flags &= ~FLAG_ALIVE;
		}

		draw_entity(entity);

		if (i == player_id) {
			if ((player->vel_x != 0 || player->vel_y != 0) && player_attack_time + 0.15 < now) {
				if (player_step_time + 0.1 < now) {
					player_walk_cycle++;
					if (player_walk_cycle > 3)
						player_walk_cycle = 0;
					player_step_time = now;
					player->sprite = &anim_player_walk[player_walk_cycle];
				}
			} else {
				player->sprite = &sprites[ENTITY_PLAYER];
			}

			if (player_attack_time + 0.05 > now) {
				player->sprite = &anim_player_attack[0];
			} else if (player_attack_time + 0.25 > now) {
				player->sprite = &anim_player_attack[1];
			}

			if (player_attack_time == now) {
				float d;
				/* find enemies nearby and kill them */
				for (i = 0; i < entity_count; ++i) {
					d = sqrt(pow(entity_data[i].x - player->x, 2) + pow(entity_data[i].y - player->y, 2));
					if (d < 20 && (entity_data[i].flags & FLAG_ENEMY)) {
						entity_data[i].hp--;
					}
				}
			}
 		}

		if (entity->flags & FLAG_ENEMY) {
			if (sqrt((player->x - entity->x) * (player->x - entity->x) +
				 (player->y - entity->y) * (player->y - entity->y)) <= sprites[player->type].width * 0.5f) {
				entity->flags &= ~FLAG_ALIVE;
				player->hp--;
			}
				 /* player->flags &= ~FLAG_ALIVE; */

			/* vision update
			if ((entity->flags & (FLAG_CAN_SEE_PLAYER)) == 0) {
				continue;
			} */

			if (entity->type == ENTITY_SKELETON_ARCHER) {
				if (entity->shot_projectile_time + 0.25 > now)
					entity->sprite = &anim_skele_archer_shoot[0];
				else if (entity->shot_projectile_time + 0.05 > now)
					entity->sprite = &anim_skele_archer_shoot[0];
				else
					entity->sprite = &sprites[ENTITY_SKELETON_ARCHER];
			}

			if ((entity->flags & (FLAG_USES_PROJECTILES)) != 0 && timers[i] <= 0) {
				unsigned id = spawn_enemy(entity->projectile, entity->x, entity->y);
				float m, x, y;

				entity->shot_projectile_time = now;
				entity_data[id] = entity_defaults[entity->projectile];
				entity_data[id].sprite = &sprites[entity->projectile];
				entity_data[id].x = entity->x;
				entity_data[id].y = entity->y;
				entity_data[id].flags |= FLAG_ENEMY;
				timers[i] = entity->projectile_cooldown;
				x = player->x - entity->x;
				y = player->y - entity->y;
				m = sqrt(pow(x, 2) + pow(y, 2));
				entity_data[id].vel_x = (x / m) * entity_data[id].speed;
				entity_data[id].vel_y = (y / m) * entity_data[id].speed;
				entity_data[id].rot = atan2(y, x);
				continue;
			}
		}

		if (entity->hp <= 0) {
			if (entity->type == ENTITY_SKELETON_ARCHER)
				skele_count--;
			entity->flags &= ~FLAG_ALIVE;
		}
	}

	if (player->x > SCR_WIDTH)
		player->x = SCR_WIDTH;

	if (player->x < 0)
		player->x = 0;

	if ((player->flags & FLAG_ALIVE) == 0)
		reset();

	if (skele_count <= 0) {
		win();
	}
}

int main(void)
{
	double dt;
	double time_last_frame = glfwGetTime();

	init();

	while (!glfwWindowShouldClose(window)) {
		now = glfwGetTime();
		dt = now - time_last_frame;
		process_input(window);

		glClearColor(0.1f, 0.0f, 0.0f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT);

		game_loop(dt);

		glfwSwapBuffers(window);
		glfwPollEvents();
		time_last_frame = now;
	}

	return 0;
}

