#version 450 core
out vec4 frag_colour;

in vec4 v_colour;
in vec2 v_tex_coords;

uniform sampler2D u_texture;

void main()
{
	frag_colour = texture(u_texture, v_tex_coords) * v_colour;
}

